# P-Sketch

In large data streams consisting of sequences of data items, those appearing over a long period of time are regarded as persistent. Compared with frequent items, persistent items do not necessarily hold large amounts of data and thus may hamper the effectiveness of vanilla volume-based detectors. Identifying persistent items plays a crucial role in a range of areas such as fraud detection and network management. Fast detection of persistent items in massive streams is however challenging due to the inherently high data rates, while state-of-the-art persistent item lookup solutions routinely require large enough memory to attain high accuracy, which questions the feasibility of deploying them in practice. In this paper, we introduce P-Sketch, a novel approach to persistent item lookup that achieves high accuracy even with small memory (L1 Cache) budgets and maintains high update speed across different settings. Specifically, we introduce the concept of arrival continuity _hotness_ that counts the number of consecutive windows in which an item appears, to effectively protect persistent items from being wrongly replaced by non-persistent ones. Through meticulous data analysis, we also reveal that items with higher persistence tend to possess a stronger hotness than non-persistent ones. Thus, we harness the information of persistence and hotness, and employ a probability-based replacement strategy to achieve a good balance between memory efficiency, lookup accuracy, and update speed. We also present a theoretical analysis of the performance of the proposed P-Sketch. Through trace-driven emulations, we demonstrate that our P-Sketch yields average F1 score and update throughput gains of up to 10.32× and respectively 2.9×, over existing schemes. Our implementation of P-Sketch is built upon the open-source code of MV-Sketch. We would like to extend our sincere appreciation to the original authors for their valuable contributions, which served as the foundation for our work.

## Compile and Run the examples
P-Sketch is implemented in C++. Follow the instructions below to compile the examples on Ubuntu using g++ and make.

### Requirements
Before proceeding, please make sure your system meets the following requirements:

- g++ and make are installed. We tested P-Sketch with g++ version 9.4.0 on Ubuntu 20.04.

- The libpcap library is installed. Most Linux distributions include libpcap and can be installed using package managers like apt-get in Ubuntu.

### Dataset

- Download the pcap file and specify the path of each pcap file in the iptraces.txt file.

### Compile
- To compile the examples, use the following commands with make:

```
    $ make main_hitter
```
  

### Run
- To run the examples, execute the following commands. The program will output statistics about the detection accuracy.

```
     $ ./main_hitter
```
